#!/usr/bin/env perl

use strict;
use warnings;

use Cwd qw(realpath getcwd);
use English qw( -no_match_vars );
use File::Basename;
use File::Path;
use FindBin qw($RealBin $RealScript);
use Getopt::Long qw(GetOptionsFromArray);
use Pod::Usage;

my $newInstall = 0;

my %opts;
my $dotfilesDirectory;
my $home;

my $remoteRepo = "http://code.stevebuzonas.com/dotfiles-perl.git";

my $commands = {
    'install' => sub {
        INFO("Installing dotfiles..." . ( $opts{'dry-run'} ? ' (dry run)' :'' ) );
        DEBUG("Running in [$RealBin] and installing in [$home]");

        install(_abs_repo_path($home,$dotfilesDirectory),$home);
    }
};

sub install {
    my($sourceDirectory,$targetDirectory,$options) = @_;
    _prepare_install($sourceDirectory,$targetDirectory,$options);
}

_run_dotfiles($RealBin, $RealScript, @ARGV) unless defined caller;

sub _run_dotfiles {
    my($realbin, $realscript, @argv) = @_;

    %opts = ();

    my $command;

    if (scalar(@argv) == 0 || $argv[0] =~ /^-/) {
        if (grep { exists $commands->{$_} } @argv ) {
            ERROR("The command should be first.");
            exit(-2);
        }
        $command = 'help';
    } else {
        $command = $argv[0];
    }

    if (exists $commands->{$command}) {
        Getopt::Long::Configure('pass_through');
        GetOptionsFromArray(\@ARGV, \%opts, 'verbose', 'quiet', 'dry-run', 'help', 'version');
        Getopt::Long::Configure('no_pass_through');
    }

    $home = realpath($ENV{HOME});

    if ($ENV{'DOTFILES_REPO'}) {
        $dotfilesDirectory = $ENV{'DOTFILES_REPO'};
        $dotfilesDirectory =~ s/$home\///;
    } elsif ($realscript eq "-") {
        # dotfiles is being invoked from stdin, not a repo; prompt the user
        # for the installation path
        INFO("Invocation from STDIN detected, treating as new installation.");
        $newInstall = 1;
        $dotfilesDirectory = &_prompt_user_statement("Repository path", "$ENV{HOME}/.dotfiles");
        $dotfilesDirectory = glob($dotfilesDirectory);
        $dotfilesDirectory =~ s/$home\///;
    } else {
        $dotfilesDirectory = $realbin;
        $dotfilesDirectory =~ s/$home\///;
        $dotfilesDirectory =~ s/\/bin//;
    }

    DEBUG("Dotfiles Directory: $dotfilesDirectory");

    if (exists $commands->{$command}) {
        if ($opts{'help'}) {
            $commands->{'help'}->([$command]);
        } elsif ($opts{'version'}) {
            show_version();
        } else {
            shift(@argv);
            $commands->{$command}->(\@argv);
        }
    } elsif ($newInstall == 0) {
        _run_git(@argv);
    }
}

sub ERROR {
    printf "ERROR: %s\n", shift;
}

sub WARN {
    printf "WARN: %s\n", shift;
}

sub INFO {
    printf "INFO: %s\n", shift if !$opts{quiet};
}

sub DEBUG {
    printf "DEBUG: %s\n", shift if $opts{verbose};
}

sub _prepare_install {
    my($sourceDirectory,$targetDirectory,$options) = @_;

    DEBUG("Installing from $sourceDirectory into $targetDirectory");

    my $symlinkRoot = _find_symlink_root($sourceDirectory,$targetDirectory);

    my $backupDirectory = $targetDirectory . '/.backup';
    DEBUG("Backup Directory: $backupDirectory");

    my $initialCwd = getcwd();
    chdir($targetDirectory);

    if ($newInstall == 1) {
        my $generateKey = (defined $ENV{'SSH_CLIENT'} || defined $ENV{'SSH_TTY'})
            ? &_prompt_user_y_or_n("Generate SSH key", "n")  # prefer ssh key forwarding on remote machines
            : &_prompt_user_y_or_n("Generate SSH key", "y");

        if ($generateKey == 1) {
            # Generate an SSH key for the current machine
            INFO("Generating SSH Key") if !$opts{'dry-run'};
            my $keyFile = &_prompt_user_statement("Enter file in which to save the key", "$ENV{HOME}/.ssh/id_rsa");
            my $keyPath = dirname($keyFile);
            mkpath($keyPath) if !$opts{'dry-run'};
            my @keygen_args = ('-f', $keyFile);
            system('ssh-keygen', @keygen_args) if !$opts{'dry-run'};

            # Get GitHub token to add SSH public key
            my $githubToken;
            if ($ENV{'GITHUB_TOKEN'}) {
                $githubToken = $ENV{'GITHUB_TOKEN'};
            } else {
                # Upload SSH public key to GitHub
                print "Create a GitHub user token by navigating to https://github.com/settings/tokens/new\n";
                print "The required permissions are `write:public_key' and `read:public_key'";
                $githubToken = &_prompt_user_statement("GitHub User Token");
            }

            open my $pub_key_fh, '<', "$keyFile.pub";
            my $pub_key = <$pub_key_fh>;

            my($type, $fingerprint, $comment) = split(q{ }, $pub_key);
            my $payload = '{"title":"' . $comment . '","key":"' . $type . ' ' . $fingerprint .'"}';
            my @github_args = ('-u', "$githubToken:x-oauth-basic", '--data', "'$payload'", 'https://api.github.com/user/keys');
            push(@github_args, '> /dev/null') if $opts{quiet};
            my $command = 'curl ' . join(' ', @github_args);
            DEBUG("Running $command");
            system($command) if !$opts{'dry-run'};
        } else {
            DEBUG("Skipping SSH key generation");
        }

        # Clone dotfiles repository
        INFO("Checking out dotfiles repository...") if !$opts{'dry-run'};
        my $repoPath = _abs_repo_path($home,$sourceDirectory);
        DEBUG("Checking out [$remoteRepo] into $repoPath");
        my @git_args = ('clone', $remoteRepo, $repoPath);
        push(@git_args, '--quiet') if !$opts{verbose};
        _run_git(@git_args) if !$opts{'dry-run'};
    }

    if (!-e $backupDirectory) {
        DEBUG("Creating $backupDirectory") if !$opts{'dry-run'};
        mkdir($backupDirectory) if !$opts{'dry-run'};
    }

    chdir($initialCwd);
}

sub _prompt_user_y_or_n {
    my($promptString,$defaultValue) = @_;

    if ($defaultValue =~ m/^y/i) {
        $promptString = "$promptString (Y/n): ";
        $defaultValue = "y";
    } elsif ($defaultValue =~ m/^n/i) {
        $promptString = "$promptString (y/N): ";
        $defaultValue = "n";
    } else {
        $promptString = "$promptString: ";
    }

    return _prompt_user($promptString,$defaultValue) =~ m/^y/i ? 1 : 0;
}

sub _prompt_user_statement {
    my($promptString,$defaultValue) = @_;

    if ($defaultValue) {
        $promptString = "$promptString [$defaultValue]: ";
    } else {
        $promptString = "$promptString: ";
    }

    return _prompt_user($promptString,$defaultValue);
}

sub _run_git {
    my @args = @_;

    my $initialDirectory = getcwd();
    my $repo = _abs_repo_path($home,$dotfilesDirectory);

    DEBUG('Running git ' . join(' ', @args) . " in $repo");
    chdir($repo);
    system('git', @args);

    chdir($initialDirectory);
}

sub _abs_repo_path {
    my($home, $repo) = @_;

    return File::Spec->file_name_is_absolute($repo)
        ? $repo
        : $home . '/' . $repo;
}

sub _find_symlink_root {
    my($source,$target) = @_;

    return ((File::Spec->splitdir($source))[1] ne (File::Spec->splitdir($target))[1])
        ? $source
        : File::Spec->abs2rel($source, $target);
}

sub _find_relevant_files {
    my($installPath) = @_;

    my $fileAttributes = {
        skip => {
            '.'          => 1,
            '..'         => 1,
            '.gitignore' => 1,
            '.git'       => 1,
        },
        recurse => [],
        execute => [],
        chmod   => {},
    };

    if (-e $installPath) {
        open(my $skip_fh, '<', $installPath);
        foreach my $line(<$skip_fh>) {
            chomp($line);
            if (length($line)) {
                my($filename,@options) = split(q{ }, $line);
                DEBUG("file $filename has @options");
            }
        }
    }
}

sub _prompt_user {
    my($promptString,$defaultValue) = @_;

    print $promptString;

    open my $tty, "<", "/dev/tty";
    my $input = <$tty>;

    chomp($input);

    if ("$defaultValue") {
        return $input ? $input : $defaultValue;
    } else {
        return $input;
    }
}

my $man = 0;
my $help = 0;
## Parse options and print usage if there is a syntax error,
## or if usage was explicitly requested.
GetOptions('help|?' => \$help, man => \$man) or pod2usage(2);
pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

## If no arguments were given, then allow STDIN to be used only
## if it's not connected to a terminal (otherwise print usage)
pod2usage("$0: No files given.") if ((@ARGV == 0) && (-t STDIN));

1;

__END__

=head1 NAME

dotfiles - A script to install the dotfiles repository of Steve Buzonas

=head1 SYNOPSIS

dotfiles <command> [--version] [--dry-run] [--verbose] [--quiet] [<args>]

=over 4

=item B<install>   Install dotfiles

=item B<import>    Add a new dotfile to the repo

=item B<package>   Handle packages managed by dotfiles

=item B<uninstall> Uninstall dotfiles

=item B<update>    Fetch updates but don't merge them in

=back

Any git command can be run on the dotfiles repository by using the following
syntax:

=over 4

dotfiles [git subcommand] [git options]

=back

See 'dotfiles help <command>' for more information on a specific command.

=cut
